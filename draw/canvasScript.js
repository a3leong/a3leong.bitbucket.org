var ctx;
var drawingCanvas;
var isDrawing = false;

function init() {
drawingCanvas = document.getElementById('drawingCanvas');

  if(drawingCanvas.getContext) {
    // Initaliase a 2-dimensional drawing ctx
    ctx = drawingCanvas.getContext('2d');

  // set listener to resize canvase if browser is resized
  // or orientation is flipped on mobile
  window.addEventListener('resize', resizeCanvas, false);
  window.addEventListener('orientationchange', resizeCanvas, false);
  resizeCanvas();

  // Setup touch event listeners for mobile
  drawingCanvas.addEventListener('touchstart',doTouchStart,false);
  drawingCanvas.addEventListener('touchend',doTouchEnd,false);
  drawingCanvas.addEventListener('touchmove',doTouchMove,false);

  // Setup default values
  ctx.lineWidth = 5;
  ctx.lineJoin = ctx.lineCap = 'round';
  ctx.strokeStyle="#FF0000";
  ctx.beginPath();  // Start a new path
  }
  else {
    alert('load failed');
  }
}

init();

// Listener functions for mouse
drawingCanvas.onmousedown = function(e) {
  isDrawing = true;
  ctx.moveTo(e.pageX, e.pageY);
};

drawingCanvas.onmousemove = function(e) {
  if( isDrawing ) {
    ctx.lineTo(e.pageX, e.pageY);
    ctx.stroke();
  }
};

drawingCanvas.onmouseup = function() {
  isDrawing = false;
};


// Functions go here

// Resizes canvas and scales drawing to match the new canvas size.
// Taken from mobiforge.com/design-development/html5-mobile-web-canvas
function resizeCanvas() {
  // Resize original canvas to necessary specs
  drawingCanvas.width = window.innerWidth;
  drawingCanvas.height = window.innerHeight - window.innerHeight/10;
}

function setColor(color) {
  // End current path
  ctx.closePath();

  if( color == 'black' )
    ctx.strokeStyle = '#000000';
  if( color == 'white' )
    ctx.strokeStyle = '#FFFFFF';
  if( color == 'yellow' )
    ctx.strokeStyle = '#FFFF00';
  if( color == 'orange' )
    ctx.strokeStyle = '#FF6600';

  ctx.beginPath(); // Start new object so colors change
};

function doTouchStart(e) {
  e.preventDefault();
  isDrawing = true;
  ctx.moveTo(e.targetTouches[0].pageX, e.targetTouches[0].pageY);
}

function doTouchMove(e) {
  if( isDrawing ) {
    ctx.lineTo(e.targetTouches[0].pageX, e.targetTouches[0].pageY);
    ctx.stroke();
  }
}

function doTouchEnd(e) {
  isDrawing = false;
}