<?php
/**
 * getArchiveContents.php
 * 
 * Description: A php script used to extract all the file names of a zip or rar archive
 *              and return a json with an array of strings with the file name information of all 
 *              the files as well as an array of all the extensions with a ("."+value) format.
 *
 * Return Value: Returns a JSON object with the fields->
 *               	archiveContents: An array of filenames in the archive file
 *                  fileExtensions:  An array of file extensions found in the archive file
 **/

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);



$archive_file = $_FILES["upload_file"]["tmp_name"]; // File to read
$file_name = $_FILES["upload_file"]["name"];
$file_list = array();    // List of files found
$extension_list = array(); // UNIQUE array of the extensions found in the files
$pathparts = pathinfo($file_name);

// Determine the file extension of the archive (.zip and .rar only). Return error if neither
if($pathparts['extension'] === 'rar') {
	// Handle .rar case
	$rar_file = RarArchive::open($archive_file);
	if($rar_file != FALSE) {          // not !== because success does not return bool type
		$list = $rar_file->getEntries();
		foreach($list as $entry) {
			// Get read filename from entry
			$entry_name = $entry->getName();
			// Push filename onto array
			array_push($file_list, $entry_name);

			// Extract the file extension and add to extension_list if UNIQUE
			$path_info = pathinfo($entry_name);
			if(isset($path_info['extension']))   // Checks if has file extension, do not try to add if none exists
				if(!in_array(strtolower($path_info['extension']),$extension_list))
					array_push($extension_list, $path_info['extension']);
		}
		// Close the rar then echo the JSON for a return
		rar_close($rar_file);
		echo json_encode(array('fileList'=>$file_list, 'extensionList'=>$extension_list));
	}
	else {
		// Handle opening error
		echo json_encode(array('scriptStatus'=>'3'));
	}
}
else if($pathparts['extension'] === 'zip') {
	// Handle .zip case
	$zip = new ZipArchive;
	$status = $zip->open($archive_file);
	if ( $status === TRUE ) {
	    for($i=0;$i<$zip->numFiles;$i++)
	    {
			// Get read filename from entry
			$entry_name = $zip->getNameIndex($i);
			// Push filename onto array
			array_push($file_list, $entry_name);

			// Extract the file extension and add to extension_list if UNIQUE
			$path_info = pathinfo($entry_name);
			if(isset($path_info['extension']))   // Checks if has file extension, do not try to add if none exists
				if(!in_array(strtolower($path_info['extension']),$extension_list))
					array_push($extension_list, $path_info['extension']);
	    }
	    $zip->close();
		echo json_encode(array('fileList'=>$file_list, 'extensionList'=>$extension_list));
	} else {
		// Handle opening error
    	echo json_encode(array('scriptStatus'=>$status));
	}

}
else {
	// Return error, unsupported file format
	echo json_encode(array('scriptStatus'=>'1'));
}