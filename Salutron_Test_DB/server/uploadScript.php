<?php
/**
 * Created by IntelliJ IDEA.
 * User: remi
 * Date: 17/01/15
 * Modified by Aaron 6/25/15, 11:40
 * Time: 11:41
 */

$flattenDirectory = TRUE;

$base_directory = "scripts/";
$target_dir = $base_directory . basename($_FILES["upload_file"]["name"]);

// Upload the file
if (isset($_FILES['upload_file'])) {
    // Example:
    if(move_uploaded_file($_FILES['upload_file']['tmp_name'], $target_dir)){
        //echo $_FILES['upload_file']['name']. " upload ...";
        echo json_encode(array('scriptStatus'=>'1'),JSON_NUMERIC_CHECK);
    	exit;
    } else {
    	echo json_encode(array('scriptStatus'=>'2'),JSON_NUMERIC_CHECK);
    	exit;
        //echo $_FILES['upload_file']['name']. " NOT upload ..."; // TODO set to true or false echo for handling
    }
} else {
	echo json_encode(array('scriptStatus'=>'3'),JSON_NUMERIC_CHECK);
	exit;
}