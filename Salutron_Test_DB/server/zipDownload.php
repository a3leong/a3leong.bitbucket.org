<?php
// Initialize Parse SDK first
// require 'autoload.php';
// use Parse\ParseObject;
// use Parse\ParseQuery;
// use Parse\ParseACL;
// use Parse\ParsePush;
// use Parse\ParseUser;
// use Parse\ParseInstallation;
// use Parse\ParseException;
// use Parse\ParseAnalytics;
// use Parse\ParseFile;
// use Parse\ParseCloud;
// use Parse\ParseClient;

// $app_id = 'gAgrPuYItYKM70hzook1k5dYjZp83qTxTfNkJ2xc';
// $rest_key = 'oIBqQc59XDAd51KIMOGhWETP5M3uM2VG0px6af2M';
// $master_key = '57WPNK8Ulaq3uQebWKp9gxJlo7YPVe1nZpLBYXAF';
// ParseClient::initialize( $app_id, $rest_key, $master_key );



// $fileId = $_GET['id'];  // obtain folder path from ajax formdata

// $query = new ParseQuery("File");
// try {
//   $file = $query->get($fileId);
//   // The object was retrieved successfully.
//   $the_folder = $file->get('fileLocation');
// } catch (ParseException $ex) {
//   // The object was not retrieved successfully.
//   // error is a ParseException with an error code and message.
// }

//$the_folder = "uploads/desktopImages"; // Manual filepath entry, do not use in production
$the_folder = $_GET['fLoc'];

// Get file ID for parse query
//$fileId = $_GET['id'];  // obtain folder path from ajax formdata



$filename = substr($the_folder,strrpos($the_folder,'/')+1);
$zip_file_name = $filename . '.zip';
$zip_file_location = 'temp/' . $zip_file_name;

//$zip_file_location = $_FILES['downloadName'];
$download_file = true;
$delete_file_after_download = true;



// Get real path for our folder
$rootPath = realpath($the_folder);

// Initialize archive object
$zip = new ZipArchive();
$zip->open($zip_file_location, ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
    }
}

// Zip archive will be created only after closing object
$zip->close();

// Force a download of the created file then delete the temp file
if ($download_file && $zip_file_location) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename='.basename($zip_file_name));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($zip_file_location));
    ob_clean();
    flush();
    readfile($zip_file_location);

    // Delete temporary zip file created if specified
    if($delete_file_after_download) {
        unlink($zip_file_location);
    }
}