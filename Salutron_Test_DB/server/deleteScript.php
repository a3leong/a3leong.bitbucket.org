<?php
/*
 * Function taken from php website under rmdir documentation, simply unlinks the file to delete it
 */
ignore_user_abort(true);
set_time_limit(0); // disable the time limit for this script

$path = $_POST['scriptLoc']; // change the path to fit your websites document structure

if( unlink($path) ) {
    echo json_encode(array('scriptStatus'=>'1'),JSON_NUMERIC_CHECK);
}
else {
    echo json_encode(array('scriptStatus'=>'0', 'err'=>"ERR: Could not delete file."),JSON_NUMERIC_CHECK);
}