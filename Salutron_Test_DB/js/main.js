    
// Initialize dependency (Parse backend)
Parse.initialize("gAgrPuYItYKM70hzook1k5dYjZp83qTxTfNkJ2xc",
				 "yhBYc0uqHZhSFRvbIrXP49XB1R4NNHeguGO39yhh");

///////////////////////////////////////
//                                   //
//  Misc. Functions (Non-category)   //
//                                   //
///////////////////////////////////////
/**
 * redirectToIndex()
 * Description: Returns the user to the index page.
 **/
function redirectToIndex() {
	document.location.href = "index.html";
}

/**
 * setAvailableTags()
 * Description: Takes a tagit and sets the available tags.
 *              This might actually be making an entirely new Tagit instance,
 *              so that might be helpful information when debugging because it would
 *              explain the cause of resetting parameters of a tagit instance.
 **/
function setAvailableTags(selector, tags) {
	$(selector).tagit({
		availableTags: tags
	});
};

/**
 * isValidScriptUpload()
 * Description: Checks if a script file is valid by checking size and extension.
 * Inputs:
 *   file - File object to check (is File JS object but should be a script file)
 * Return Value: Returns true if valid file ready for upload, false otherwise.
 **/
function isValidScriptUpload(file) {
	var max = 41943040;      // Max upload file size (limited by PHP server to 40M by php.ini at the moment)
	// Check extension (rar or zip only)
	var ext = getExtension(file.name);
	if( ext === null )        // Check if has extension
		return false;
	else if( ext !== ".py" && ext !==".pyo" && ext !== ".pyc" ) // Check for correct extension name
		return false;
	else if(file.size > max)                                      // Extension checks out, check for name in DB
		return false;
	else
		return true;
}

/**
 * isValidUpload()
 * Description: Checks whether the file going to be uploaded is valid or not
 *              by checking factors such as extension and whether the file already
 *              exists in the database or not (based on name). Returns an int value.
 * Inputs:
 *    file - File JS object to validate. Not a Parse File Object
 * Return Value: Returns a positive int if the file is ok to upload (may still fail at php level due
 *               uncovered use cases), returns 0 if there is something wrong with the file. Returns
 *               -1 if the file is already in the parse DB (meaning prompt for overwrite).
 **/
function isValidUpload(file) {
	var max = 41943040;      // Max upload file size (limited by PHP server to 40M by php.ini at the moment)
	return new Promise(function(resolve,reject) {
		// Check extension (rar or zip only)
		var ext = getExtension(file.name);
		if( ext === null )        // Check if has extension
			resolve(0);
		else if( ext !== ".rar" && ext !==".zip" ) // Check for correct extension name
			resolve(0);
		else {                                       // Extension checks out, check for name in DB
			if(file.size > max) {                    // Check file size to make sure its not over the max allowed
				resolve(-2);
			}
			var filename = file.name;
			var FileClass = Parse.Object.extend("File");
			var query = new Parse.Query(FileClass);
			query.equalTo('filename',removeExtension(filename));
			query.find().then(function(result) {
				// Check result list size, will be 0 if no matches, which means valid
				if(result.length === 0)
					resolve(1);
				else
					resolve(-1);
			}, function(err) {
				resolve(0);
			});
		}
	});
}

/**
 * downloadLogFile()
 * Description: Creates a text file with a log of the output of a Python script result.
 *              Takes different streams and formats all the data into a legible format.
 * Inputs:
 *   fileLocations - Array of all file locations used when the Python script was called
 *   scriptName    - Name of the Python script that was run
 *   result        - Array of fileLocations that succeeded boolean test 
 *                   (all files if no boolean test in script)
 *   out           - The output stream of the Python script, should have
 *                   already been formatted to have a section for each file
 *                   divided by new line chars
 *   err           - The errOut stream of the Python script.
 * Return Value: None, but the user should have a download automatically started in their browser for the
 *               newly created file.
 **/
function downloadLogFile(fileLocations,scriptName,result, out, err) {
	// Create file name using scriptname, date, and files
	var d = new Date();
	var date_formatted = d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear();
	var formatScriptName = scriptName.substring(0,scriptName.lastIndexOf('.')); // Truncate file extension of python script
	var filenames = "";
	for( url of fileLocations ){    // Strip filenames from location
		filenames = filenames + '_' + (url.substring( (url.lastIndexOf('/')+1), url.length ));
	}
	var outFilename = date_formatted + "-" + formatScriptName + "-" + filenames;
	// // Create text info component using given variables
	var text = "";
	var text = "Script: " + scriptName + "\r\nFiles: " + filenames + "\r\nDate: " + date_formatted + "\r\n\r\n";
	var text = text + "Result Files:\r\n" + result + "\r\n";
	var text = text + "Output:\r\n" + out + "\r\n";
	var text = text + "ErrOut:\r\n" + err + "\r\n";

	// Create download element and progromatically click it then remove it
	// Code modified from a stack overflow post
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	element.setAttribute('download', outFilename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
}

/**
 * getTagitEditRemovals()
 * Description: Compares the original(passed in to avoid more queries) tagit assigned values with the newly
 *              submitted tagit array. If the originalTags element is not found in the newTags array, it means
 *              that that tag was removed in the edit.
 * Inputs:
 *     origArray - An array of elements
 *     newArray  - An array of different elements. If old elements not contained in new, put those elements in nonContainedElements array
 * Return value: Returns a list of tags that were removed since the new submission of a tagit array. THis
 *               value is to be taken and used to remove a Tags relationships from a File object later.
 **/
function getNonContainedElements(origArray,newArray) {
	var nonContainedElements = [];
	for(e of origArray) {
		if(newArray.indexOf(e)===(-1)) { // -1 return means the old tag no longer exists in the tagit function
			nonContainedElements.push(e) // Push the removed tag to the return array
		}
	}
	return nonContainedElements;
}


///////////////////////////////////////
//                                   //
//   String manipulation Functions   //
//                                   //
///////////////////////////////////////
/**
 * getURLFileId()
 * Description: Takes a url and strips the file id
 *              off of it. To be used whhen the id is embedded
 *              in the url like in fileInfoPage.html
 * Inputs:
 *    url - The url String to substring.
 * Return value: Returns the substring containing a Files id
 **/
function getURLFileId(url) {
	if(url.indexOf('?id=')===(-1))
		return null;
	else
		return url.substring(url.indexOf('?id=')+4,url.length);
}

/**
 * removeExtension()
 * Description: String manipulation function to get a filename without the extension.
 *              Note: This will screw things up if you don't have a file extension, but
 *                    then why would you be calling this function?
 * Inputs:
 *   filename - A filename to strip the extension of
 * Return Value: Returns a filename without an extension. 
 **/
function removeExtension(filename) {
    var lastDotPosition = filename.lastIndexOf(".");
    if (lastDotPosition === -1) return filename;
    else return filename.substr(0, lastDotPosition);
}

/**
 * getExtension()
 * Description: String manipulation function to get the extension
 *              of a filename. Will bug if no extension, but then why
 *              would you be calling this?
 * Inputs:
 *   filename - Filename String to substring
 * Return Value: Returns the extension (Including the '.' char!)
 **/
function getExtension(filename) {
	var lastDotPosition = filename.lastIndexOf(".");
	if( lastDotPosition === -1)
		return null;   // Return null if no extension
	else
		return( filename.substr(lastDotPosition,filename.length) )
}

///////////////////////////////////////
//                                   //
//   Parse DB Functions              //
//                                   //
///////////////////////////////////////
///////////////////////////////////
// Parse Files Class Functions
//////////////////////////////////
/**
 * searchFileByName()
 * Description: Searches for a file object in Parse and returns a promise.
 *              partial file names can be entered too since the function
 *              searches for substrings within the filenames
 * 
 * Return Value: Returns a promise to return a list of files that have a substring
 *               that matches parameter 'name'.
 **/
function searchFileByName(name) {
	return new Promise(function(resolve,reject) {
		var FileClass = Parse.Object.extend('File');
		var query = new Parse.Query(FileClass);
		query.contains('fileSearchName',name.toLowerCase()); // Search lower case permutation for more results
		query.find().then(function(fileList) {
			resolve(fileList);
		}, function(err) {
			reject(err);
		});
	});
}

/**
 * searchFileByTag()
 * Description: Look for files that contain a tag relation to all the tags.
 *              Must match all of them.
 * Inputs:
 *     tags - Tags to query for
 * Return Value: Returns a promise to return an array of parse "File" class objects that match the
 *               tag search query.
 **/
function searchFileByTag(tags) {
	return new Promise(function(resolve,reject){
		getSearchFileList(tags).then(function(fileList){
			resolve(fileList);
		}).catch(function(err){
			reject(err);
		});
	});
}

/**
 * getSearchFileList()
 * Description: Using the passed in array of strings, find
 *              rows in the Parse File class that match the relation to
 *              rows in the Tags class.
 * Inputs:
 *     tags - Tags to search files for match
 * Return Value: Returns a promise to return all File objects that match the  
 **/
function getSearchFileList(tags) {
	return new Promise(function(resolve, reject) {
		//var queryArray = []; // Array of queries to compound into one
		var TagClass = Parse.Object.extend('Tags');
		var FileClass = Parse.Object.extend('File');

		// Check for no tags, if none, return query that returns all rows of file class
		if(tags.length===0){
			var query = new Parse.Query(FileClass);
			query.ascending('filename');     // Sort search results by name
			query.find().then(function(allFileList){
				resolve(allFileList);  
				},
				function(err){
					reject(err);
				});
		}

		// Create a tag query and build it up with all the tags
		var tagQuery = new Parse.Query(TagClass);
		tagQuery.equalTo('null','null'); // Create base case value (nothing) so 'or' query does not return everything
		// Build query for Tag objects using compound OR query
		for( tag of tags )
		{
			var query = new Parse.Query(TagClass);
			query.equalTo('searchName',tag.toLowerCase());
			tagQuery = Parse.Query.or(tagQuery,query);  // Use tagQuery Like an accumulator
		}

		// Get a list of tag objects that match the description
		tagQuery.find().then(function(tagObjList){
			var fileQuery = new Parse.Query(FileClass);
			// Build query 
			for( t of tagObjList ) {
				fileQuery.equalTo('tags',t);
			}
			fileQuery.ascending('filename');  // Sort results by name
			fileQuery.find().then(function(fileObjList){
				resolve(fileObjList);
			},
				function(err){
					reject(err);
				});
			},	
			function(err) { 
				reject(err);
			});
	});
}

/**
 * modifyParseObject(file,value,key)
 * Description: Takes a file and changes the key value pair by overwriting it
 *              to the object then saving the object.
 * Inputs:
 *    file  - File Parse object to modify
 *    key   - Key in key/value pair to modify
 *    value - Value to change to 
 * Return Value: Returns a promise to return the file after modification, not needed
 *               for use, but might be helpful for clarity's sake.
 **/
function modifyParseObject(file,key,value) {
	return new Promise(function(resolve,reject){
		file.set(key,value);
		file.save().then(function(fileAgain){
			resolve(fileAgain);
		});
	});
}

/**
 * pushtoParse()
 * Description: Takes a json and creates a new Parse object in the File table.
 *              Assumes the zip folder has already been uploaded and decompressed
 * Inputs:
 *    file_json - JSON data which contains file parameters wanted 
 * Return Value:
 *    Returns a Promise to resolve. This allows for errors to be caught and the file object
 *    to be destroyed if there is an error.
 **/
function pushToParse(file_json){
	var FileClass =      Parse.Object.extend('File');
	var TagClass =       Parse.Object.extend('Tags');
	var filename =       file_json['filename'];	
	var fileSearchName = filename.toLowerCase();
	var fileLoc =        "uploads/" + filename;
	var tags =           file_json['tags'];
	var description =    file_json['description'];
	var extensions =     file_json['extensions'];

	return new Promise(function(resolve,reject) {
		// Create the file entry
		var newFile = new FileClass();
		newFile.set("filename", filename);
		newFile.set("fileLocation", fileLoc);
		newFile.set("description",description);
		newFile.set("fileExtensions",extensions);
		newFile.set("fileSearchName",fileSearchName);

		newFile.save().then(function(fileObj){
			return addTagsToFile(fileObj,tags);
		}).then(resolve()).catch(function(err){
			reject(err);      // Upload Failed, destroy the new made file 
			file.destroy();
		});
	});
}

/**
 * getFileById(id)
 * Description: Gets a Parse Object from File table by querying the File
 *              table for the entry ID. Guaranteed by the Parse DB to be unique.
 * Inputs:
 *    id - String value of the File Object id consisting of numbers and letters
 * Return Value: Returns a promise to resolve with the File Object or reject if there's an error.
 **/
function getFileById(id) {
	return new Promise(function(resolve,reject) {
		var FileClass = Parse.Object.extend('File');
		var fileQuery = new Parse.Query(FileClass);
		fileQuery.get(id).then(function(result){
			resolve(result);
		},function(err){reject(err);});
	});
}

///////////////////////////////////
// Parse Tags Class Functions
//////////////////////////////////
/**
 * getFileTags()
 * Description: Get all Tags objects from a Parse File object
 * Inputs:
 *     file - The File parse object to get the tags of
 * Return Value: Returns a promise to return an array of tag objects
 **/
function getFileTags(file){
	return new Promise(function(resolve,reject){
		var tagRelations = file.relation('tags');
		var query = tagRelations.query();
		query.equalTo("Tags");
		tagRelations.query().find().then(function(tagResult) {
			resolve(tagResult); // Resolve with the array of tags
		}, function(err){         // Catch errors
			reject(err);
		});
	});
}

/**
 * getAvailableTags()
 * Description: Get all Tags object relations to every
 *              File object in fileList.
 * Inputs:
 *   fileList - The list of Parse "File" class objects to 
 *              get the tags of.
 * Return Value: Returns a promise to return a unique element
 *               array of strings containing the display name
 *               of all the Tags found
 **/
function getAvailableTags(fileList) {
	return new Promise(function(resolve,reject) {
		var promiseArray = [];

		for(file of fileList) {
			promiseArray.push(getFileTags(file));
		}

		Promise.all(promiseArray).then(function(resultArray){
			var tagArray = [];
			for(result of resultArray) {
				for(tag of result) {
					var name = tag.get('displayName');
					if( tagArray.indexOf(name) == -1 ) {
						tagArray.push(name);
					}
				}
			}
			resolve(tagArray);
		}).catch(function(err){
			reject(err);
		});
	});
}

/**
 * getAllAvailableTags()
 * Description: Gets all the rows of the Tags class and
 *              gives all available displayNames as callback.
 *              This method should be faster than getAvailableTags()
 *
 * Return Value: Returns a promise to return an array of every single tag available.
 **/
function getAllAvailableTags() {
	return new Promise(function(resolve,reject){
		var TagClass = Parse.Object.extend('Tags');
		var tagQuery = new Parse.Query(TagClass);
		tagQuery.find().then(function(tagList){
			var tags = [];
			// Build array of names
			for(t of tagList)
			{
				if( tags.indexOf(t.get('searchName')) === -1 )
					tags.push(t.get('displayName'));
			}
			resolve(tags);
		}, function(err){         // Catch error
			reject(err);
		});
	});
}

/**
 * getTag()
 * Description: Checks if tag exists, if not creates new one.
 *              Returns the tag whether new or pre-existing.
 * Return Value: Returns the requested tag
 */
function getTag(tag) {
	return new Promise(function(resolve,reject){
		var TagClass = Parse.Object.extend("Tags");
		var tagQuery = new Parse.Query(TagClass);
		tagQuery.equalTo("searchName",tag.toLowerCase());
		tagQuery.find().then(function(result) {
			if(result.length===1) {
				resolve(result[0]);   // Tag already exists, return it
			}
			else if(result.length===0) {
				// Tag does not exist,create new one and return it
				var newTag = new TagClass();
				newTag.set('displayName',tag);
				newTag.set('searchName',tag.toLowerCase());
				resolve(newTag.save());
			}
			else {
				reject("ERR: Tag search returns too many results, please check Parse DB for duplicates.");
			}
		});
	});
}

/**
 * createTagRelation()
 * Description: Gets a tag object and creates a relation between
 *              the tag and the file.
 */
function createTagRelation(tag, file) {
	return new Promise(function(resolve,reject){
		getTag(tag).then(function(tagObj){
			var relation = file.relation('tags');
			relation.add(tagObj);
			file.save();
			resolve();
		}).catch(function(err){reject(err);});
	});
}

/**
 * addTagsToFile()
 * Description: Adds Tags object relations to a File object
 * Inputs:
 *     file - File Parse object to create tag relation with
 *     tags - Tags to create relation with File
 * Return Value: Returns a promise to resolve with no value (basically says "I'm done")
 **/
function addTagsToFile(file,tags) {
	return new Promise(function(resolve,reject){
		var promiseArray=[];
		for(t of tags) {
			promiseArray.push(createTagRelation(t,file));
		}
		Promise.all(promiseArray).then(resolve()).catch(function(error){ reject(error); });

	});
}

/**
 * removeTagRelation()
 * Description: Removes a Tag-File relation from the Parse database.
 *              Does this by finding the Tags object through searching the name
 *              then removing the relation.
 * Inputs:
 *   tag - The name of the tag object to remove from the file.
 *  file - The file object to remove the tag of
 * Return Value: Returns a promise to resolve on success or reject with err msg on failure.
 **/
function removeTagRelation(tag, file) {
	return new Promise(function(resolve,reject){
		var TagClass = Parse.Object.extend('Tags');
		var tagQuery = new Parse.Query(TagClass);
		tagQuery.equalTo('searchName',tag.toLowerCase());
		tagQuery.find().then(function(tagResult) {
			// Check length to determine result handling
			if(tagResult.length === 0) {  // No row in table found, no need to do anything
				resolve();
			}
			else if(tagResult.length === 1) { // One match found, just create relation
				return tagResult[0];          // This return statement passes along to next chained promise
			}
			else {
				reject("ERROR: Too many tags objects found: " + tag);
			}
		},function(err){
			return;
		}).then(function(tagObj){
			if(tagObj!==null) {
				var relation = file.relation('tags');
				relation.remove(tagObj);
				file.save();
				resolve();
			}
			else {
				reject("ERROR: tagObj is null: " + tag);
			}
		});
	});
}

/**
 * removeTagsFromFile()
 * Description: Removes multiple tags from a file using Promise.all
 *              on an array of promises. Extends removeTagRelation to handle
 *              Multiple tags for removeTagRelation()
 * Inputs: 
 *    file - The parse file object
 *    tags - An array of displayNames of Tags objects (Array of strings)
 * Return Value: Returns a promise to resolve on success or reject on failure.
 **/
function removeTagsFromFile(file, tags){
	var FileClass = Parse.Object.extend('File');
	return new Promise(function(resolve,reject) {
		var promiseArray=[];
		for(t of tags)
		{
			promiseArray.push(removeTagRelation(t,file));
		}
		Promise.all(promiseArray).then(resolve()).catch(function(err){ reject(err); });    // Async all the functions then resolve when all done
	});
}

/**
 * removeTagsFromFiles()
 * Description: Extends removeTagRelation() and removeTagsFromFile() by
 *              using Promise.all.
 * Inputs:
 *    fileList - An array of File Parse objects to remove tags from
 *    tags     - An array of Tags Parse objects displayNames
 * Return Value: Returns a promise to resolve on success or reject on failure.
 **/
function removeTagsFromFiles(fileList, tags) {
	return new Promise(function(resolve,reject){
		var promiseArray = [];
		// Traverse file array 
		for( file of fileList ) {
			promiseArray.push(removeTagsFromFile(file,tags));
		}
		Promise.all(promiseArray).then(function() {
			resolve();
		}).catch(function(err){ reject(err); });
	});
}

/**
 * getFileTagListDiff()
 * Description: Gets the difference between a File Object's Tags relations
 *              and new tags. Then generates relationships between any
 *              File/Tags relations the File object does not have.
 * Inputs: 
 *    file    - File Parse object
 *    newTags - Array of tags 
 * Return Value: Returns a promise to resolve or reject if there's an error.
 **/
function getFileTagListDiff(file, newTags){
	return new Promise(function(resolve,reject){
		getTagsFromFile(file).then(function(tagList){
			var addTagArray = getNonContainedElements(newTags,tagList);
			return addTagArray;            // Error begins on passing this
		}).then(function(passedArrayTags){  
			addTagsToFile(file,passedArrayTags);
		}).then(function(){
			resolve();
		}).catch(function(err){reject(err);});
	});
}

/**
 * addTagsToFiles()
 * Description: Adds multiple tags to multiple files.
 *              getFileTagListDiff() function extension.
 *              First generates new Tags objects if they do not exist,
 *              then creates relationships between all newTags and files in fileArray.
 * Inputs:
 *    fileArray - Array of files to add Tags relationships to
 *    newTags   - Array of String representation of Tags objects
 * Return Value: Returns a promise to resolve on successs or reject with error msg on failure.
 **/
function addTagsToFiles(fileArray, newTags) {
	return new Promise(function(resolve,reject){
		// Add new tags to database if they don't exist, this takes care of
		// issues with parallel computing creating duplicates
		var tagPromiseArray = [];
		for(tag of newTags) {
			tagPromiseArray.push(getTag(tag));
		}

		Promise.all(tagPromiseArray).then(function(){
			var promiseArray = [];
			for(f of fileArray) {
				promiseArray.push(getFileTagListDiff(f, newTags));
			}

			Promise.all(promiseArray).then(function(){
				resolve();
			}).catch(function(err){reject(err);});
		});
	});
}

/**
 * getTags()
 * Description: Async function to get tags from file's ID through a query.
 * Inputs:
 *    file - The Parse File object to get the Tags relations of
 * Return Value: Returns a promise to resolve with an array of tag objects
 *               that is related to the ID
 */
function getTagsFromFile(file) {
	return new Promise(function(resolve,reject) {
		var tagList = [];
		var tagRelations = file.relation('tags');
		var query = tagRelations.query();
		query.equalTo("Tags");
		tagRelations.query().find().then(function(tagResult){
			for(tag of tagResult)
			{
				tagList.push(tag.get('displayName'));      // Check not needed since tags no user input
			}
			resolve(tagList);
		});
	});
}

/**
 * getTagsFromFiles()
 * Description: Gets tags from fileList and removes all duplicates.
 * Input:   fileList - array of file objects from Parse
 * Return Value: Returns an array of tags (display names) that are unique.
 **/
function getTagsFromFiles(fileList) {
	return new Promise(function(resolve,reject){
		var promiseArray = [];
		for(file of fileList) {
			promiseArray.push(getTagsFromFile(file));
		}
		Promise.all(promiseArray).then(function(tagArrayList){   // is an array of an array of tags
			var returnTagList = [];
			// Traverse through all tags, only add if unique
			for(tagArray of tagArrayList) {            // Time cost is O(n), iterates once over all elements
				for(tag of tagArray) {
					if(returnTagList.indexOf(tag)===(-1)) {
						returnTagList.push(tag);
					}
				}
			}
			resolve(returnTagList);

		}).catch(function(err){alert(err);});
	});
}

/** 
 * removeUnusedTags() 
 * Description: A database management function used to 
 *              remove any tags that no longer share a relation
 *              with any file entry in the database. Call this
 *              function when there begins to be too much tag clutter
 *              in autocomplete. Not automatically done because unused
 *              tag does not necessarily mean it won't be used later. (TODO rethink this idea)
 *              NOT YET IMPLEMENTED
 **/
function purifyUnusedTags() {
	// var TagClass = Parse.Object.extend("Tags");
	// var FileClass = Parse.Object.extend("File");
	// var fileQuery = new Parse.Query(FileClass);
	// fileQuery.find().then(function(fileList) {
	// 	for
	// });
}

///////////////////////////////////////
//                                   //
//   AJAX Calling Functions          //
//                                   //
///////////////////////////////////////
///////////////////////////////////
// Upload and download functions
//////////////////////////////////
/**
 * downloadFolder()
 * Description: Zips the target directory (specified by location) and has the browser download it.
 *              The literal thing it does is open a php file using a URL that contains the filelocation data.
 *              Since the server handles PHP, the user never sees the PHP code and the download starts automatically.
 * Inputs:
 *    file - Parse File object to download (doesn't download the object but the actual file on the server).
 **/
function downloadFolder(file) {
	// For the sake of security, do another Parse query with the ID
	fileLocation = file.get("fileLocation");
	window.open('server/zipDownload.php?fLoc='+fileLocation);
}

/**
 * downloadFolders()
 * Description: A multi-use version of downloadFolder. Opens multiple windows by calling
 *              downloadFolder multiple times. May trigger spam prevention because it
 *              opens to many windows instantaneously so you might need to check an allow
 *              popups from this site box.
 * Inputs:
 *   fileArray - Array of File Parse objects
 **/
function downloadFolders(fileArray) {
	// Loop through all files in fileArray and call downloadFolder
	for(file of fileArray) {
		downloadFolder(file);
	}
}

/**
 * uploadFile()
 * Description: Uploads a file by calling a PHP script. File meaning a valid zip file.
 *              Also updates the progress bar here, which means this function is inseperable
 *              from uploadPage.html
 * Inputs:
 *    file - A Parse object from the "File" table
 * Return: Returns a promise to resolve on success or reject on failure.
 **/
function uploadFile(file) {
	return new Promise(function(resolve,reject) {
		var url = 'server/upload.php';
		var xhr = new XMLHttpRequest();
		var fd = new FormData();
		xhr.open("POST",url,true);
		fd.append('upload_file',file);

		xhr.onload = function() {
			// This is called even on 404 etc
			// so check the status
			if (xhr.status == 200) {
				// Resolve the promise with the response text
				try {
					var json = $.parseJSON(xhr.responseText);
				}
				catch(err) {
					reject("Error in PHP output:\n"+ err);
				}
  			  	$("#uploadMeter").css("width","0%");     // Reset bar on upload finish

  			  	if(json['scriptStatus']===1) {           // Check if script succeeded or failed
  			  		resolve();
  			  	}
  			  	else{
  			  		reject("Error in PHP script: " + json['scriptStatus']);  // Could later change to case statement
  			  	}
			}
			else {
				reject(Error(xhr.statusText));   // Reject if status is not 200 
			}
		};

		xhr.upload.addEventListener("progress", updateProgress, false);

		// progress on transfers from the server to the client (downloads)
		// Function taken from Mozilla documentation
		function updateProgress (oEvent) {
		  if (oEvent.lengthComputable) {
		    var percentComplete = parseInt(oEvent.loaded/oEvent.total*100,10);
			$("#uploadMeter").css( "width",(percentComplete.toString()+"%") );
		  } else {
		    // Unable to compute progress information since the total size is unknown
		  }
		}

		// Handle network errors
		xhr.onerror = function() {
			reject(Error("Network Error"));
		};
		xhr.send(fd);
	});
}

/**
 * uploadScript()
 * Description: Used to upload a script by calling a PHP script.
 * Inputs:
 *   file - javascript file object to pass to PHP
 * Return Value: Returns a promise to resolve on success or reject on failure.
 **/
function uploadScript(file) {
	return new Promise(function(resolve,reject) {
		var url = 'server/uploadScript.php';
		var xhr = new XMLHttpRequest();
		var fd = new FormData();
		xhr.open("POST",url,true);
		fd.append('upload_file',file);

		xhr.onload = function() {
			// This is called even on 404 etc
			// so check the status
			if (xhr.status == 200) {
				// Resolve the promise with the response text
				try {
					var json = $.parseJSON(xhr.responseText);
				}
				catch(err) {
					reject("Error in PHP output:\n"+ err);
				}
  			  	$("#scriptUploadMeter").css("width","0%");     // Reset bar on upload finish

  			  	if(json['scriptStatus']===1) {
					resolve();
  			  	}
  			  	else {
  			  		reject("Error in PHP script: " + json['scriptStatus']); // Could later use a case statement to
  			  	}															// Send a more descriptive error message

			}
			else {
				// Otherwise reject with the status text
				// which will hopefully be a meaningful error
				reject(Error(xhr.statusText));
			}
		};

		xhr.upload.addEventListener("progress", updateProgress, false);

		// progress on transfers from the server to the client (downloads)
		// Function taken from Mozilla documentation
		function updateProgress (oEvent) {
		  if (oEvent.lengthComputable) {
		    var percentComplete = parseInt(oEvent.loaded/oEvent.total*100,10);
			$("#scriptUploadMeter").css( "width",(percentComplete.toString()+"%") );
		    // ...
		  } else {
		    // Unable to compute progress information since the total size is unknown
		  }
		}

		// Handle network errors
		xhr.onerror = function() {
			reject(Error("Network Error"));
		};
		xhr.send(fd);
	});
}

/**
 * deleteScript()
 * Description: Deletes a script by calling a PHP script.
 * Inputs:
 *   scriptName - String containing the name of the script to delete.
 * Return Value: Returns a promise to resolve on success or reject on failure with an err msg.
 **/
function deleteScript(scriptName) {
	var scriptLocation = "scripts/"+scriptName;
	return new Promise(function(resolve,reject) {
		$.ajax({
			type: "POST",
			url: "server/deleteScript.php",
			data: {scriptLoc: scriptLocation}
		}).then(function(json_data){
			try {
				var data = $.parseJSON(json_data);
			}
			catch(err) {
				reject(err);
			}
			if(data['scriptStatus']) // If success, remove the file from parse database
			{
				resolve();
			}
			else{
				var err = data['err'];
				reject(err);
			}
		});
	});
}

/**
 * deleteFile()
 * Description: Deletes a file from the Parse DB and physical file from
 *              server.
 * Inputs: 
 *    file - File to delete
 * Return Value: Returns a promise to resolve on success or reject on failure with an err msg.
 **/
function deleteFile(file) {
	return new Promise(function(resolve,reject){
		// Make ajax call
		var filepath = file.get('fileLocation');
		$.ajax({
			type: "POST",
			url:  "server/deleteFile.php",
			data: {folderLoc: filepath},
		}).then(function(json_data){
			try {
				var data = $.parseJSON(json_data);
			}
			catch(err) {
				reject(err);
			}
			if(data['scriptStatus']) // If success, remove the file from parse database
			{
				file.destroy().then(function(success){
					resolve();
				},function(err){
					reject(err);
				});       // Delete file from parse database
			}
			else{
				var err = data['err'];
				reject(err);
			}
		});
	});
}

/**
 * deleteFiles()
 * Description: Extends deleteFile() by using Promise.all to 
 *              handle an array of files.
 * Inputs:
 *   fileList - Array of Parse Object Files
 * Return Value: Returns a promise to resolve on success or reject on failure with an err msg.
 **/
function deleteFiles(fileList) {
	return new Promise(function(resolve,reject) {
		// Create and build promise array
		var promiseArray = [];
		for( file of fileList ) {
			promiseArray.push(deleteFile(file));
		}
		// Run promise.all to get make sure there's a return from all function calls before doing anything
		Promise.all(promiseArray).then(function(){
			resolve();
		}).catch(function(err){reject(err);});
	});
}

///////////////////////////////////
// Read Contents Functions
//////////////////////////////////
/**
 * getArchiveContents()
 * Description: Call a php script to get a JSON obj
 *              containing the extensions and filenames.
 *              Basically uploads the file in this step before uploading again.
 *              This takes more time, but you want to see the contents of what you
 *              upload right? If not you'll have to change around some front end code too
 *              for a speed increase.
 * Inputs:
 *   file - File object class to get contents of.
 * Return Value: Returns a promise to return a JSON containing file info
 **/
function getArchiveContents(file) {
	// Return a new promise.
	return new Promise(function(resolve, reject) {
		// Do the usual XHR stuff
		var url = 'server/getArchiveContents.php';
		var xhr = new XMLHttpRequest();
		var fd = new FormData();
		xhr.open('POST', url);
		fd.append('upload_file',file);

		xhr.onload = function() {
			// This is called even on 404 etc
			// so check the status
			if (xhr.status == 200) {
				// Resolve the promise with the response text
				try {
					var json = $.parseJSON(xhr.responseText);
					resolve(json);
				}
				catch(err) {
					reject(err);
				}
			}
			else {
				// Otherwise reject with the status text
				// which will hopefully be a meaningful error
				reject(Error(xhr.statusText));
			}
		};

		// Handle network errors
		xhr.onerror = function() {
			reject(Error("Network Error"));
		};

		// Make the request
		xhr.send(fd);
	});
}

/**
 * getFolderContents()
 * Description: Finds all the files in the containing folder and returns the names
 *              as an array. This is done by calling a php script to handle server side
 *              work then parsing the returned JSON
 * Inputs:
 *    folderPath - Folder path to get contents of
 * Return Value: Returns a promise to return an 
 *               array that contains all the filenames in the folder.
 **/
function getFolderContents(folderPath) {
	return new Promise(function(resolve,reject) {
		// Get necessary data
		$.ajax({
			type: "POST",
			url:  "server/getFolderContents.php",
			data: {folderLoc: folderPath},
			success: function(response) {
            	try {
	                var output = $.parseJSON(response);
					resolve(output);
				}
				catch(err){
					reject(err);
				}
            },
            error: function(error) {
                reject(error);
            }
		});
	});
}

/**
 * callPythonScript()
 * Description: Makes an Ajax call to allow server to run a Python script.
 *              Validation is not handled here.
 * Inputs:
 *    scriptName    - Name of the script to run
 *    fileLocations - An array of all the file locations used for the script
 * Return Value: Returns a promise to return a JSON containing the Python script's output
 **/
function callPythonScript(scriptName,fileLocations) {
	return new Promise(function(resolve,reject) {
        var scriptURL = 'server/scripts/'+scriptName;
        var jsonData = {"fileLocs":fileLocations};
        $.ajax({
            url: scriptURL,
            data: {postData: JSON.stringify(jsonData)},
            type: 'POST',
            success: function(response) {
            	try {
	                var output = $.parseJSON(response);
					resolve(output);
				}
				catch(err){
					reject(err);
				}
            },
            error: function(error) {
                reject(error);
            }
	    });
	});
}

