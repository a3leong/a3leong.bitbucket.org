// Attempt to create audio context
var context;
window.onload = init;

function init() {
  try {
    // Fix up for prefixing
    window.AudioContext = window.AudioContext||window.webkitAudioContext;
	context = new AudioContext();
  }
  catch(e) {
    alert('Web Audio API is not supported in this browser');
  }
}


// Test code function
var RhythmSample = {
};

RhythmSample.play = function() {

  function playSound(buffer, time) {
    var source = context.createBufferSource();
    source.buffer = buffer;
    source.connect(context.destination);
    if (!source.start)
      source.start = source.noteOn;
    source.start(time);
  }
  
  // We'll start playing the rhythm 100 milliseconds from "now"
  var startTime = context.currentTime + 0.100;
  var tempo = document.getElementById('bpmEntry').value;
  //var tempo = 80; // BPM (beats per minute)
  var eighthNoteTime = (60 / tempo) / 2;
  
  var kick = sound;
  var snare = sound;
  var hihat = sound;


  // Play 2 bars of the following:
  for (var bar = 0; bar < 2; bar++) {
    var time = startTime + bar * 8 * eighthNoteTime;
    // Play the bass (kick) drum on beats 1, 5
    playSound(kick, time);
    playSound(kick, time + 4 * eighthNoteTime);

    // Play the snare drum on beats 3, 7
    playSound(snare, time + 2 * eighthNoteTime);
    playSound(snare, time + 6 * eighthNoteTime);

    // Play the hi-hat every eighthh note.
    for (var i = 0; i < 8; ++i) {
      playSound(hihat, time + i * eighthNoteTime);
    }
  }
};


  function playSound(buffer, time) {
    var source = context.createBufferSource();
    source.buffer = buffer;
    source.connect(context.destination);
    if (!source.start)
      source.start = source.noteOn;
    source.start(time);
  }


var isPlaying = false;                  // Prevents playRhythm sounds from overlapping
function playRhythm(bufferList){
	  // We'll start playing the rhythm 100 milliseconds from "now"
  var startTime = context.currentTime + 0.100;
  var tempo = document.getElementById('bpmEntry').value;
  //var tempo = 80; // BPM (beats per minute)
  var eighthNoteTime = (60 / tempo) / 2;
  
  var kick = bufferList['kick'];
  var snare = bufferList['snare'];
  var hihat = bufferList['hi_hat'];


  // Play 2 bars of the following:
  for (var bar = 0; bar < 2; bar++) {
    var time = startTime + bar * 8 * eighthNoteTime;
    // Play the bass (kick) drum on beats 1, 5
    playSound(kick, time);
    playSound(kick, time + 4 * eighthNoteTime);

    // Play the snare drum on beats 3, 7
    playSound(snare, time + 2 * eighthNoteTime);
    playSound(snare, time + 6 * eighthNoteTime);

    // Play the hi-hat every eighthh note.
    for (var i = 0; i < 8; ++i) {
      playSound(hihat, time + i * eighthNoteTime);
    }
  }
  alert("done playing");
  isPlaying = false;
	
}


// Handler for play button
function soundButt() {
	// Load sounds and play function when calling this method
	// TODO: Type check for bpm input (int only)
	if(isPlaying==false) {
		isPlaying = true;
		var loader = new AbbeyLoad([{'kick':'kick.wav','snare':'snare.wav','hi_hat':'hi_hat.wav'}],playRhythm);
	}
	else
		alert("Error: Already playing");
}



// Code for detecting user input's bpm





function calculateAverageBPM(timeSampleArray)
{
	/**
		CalculateBPM
		Calculates the BPM value based on the last button press and current
	**/
	function calculateBPM(lastTime,currentTime)
	{
		console.log(60000/(currentTime - lastTime));
		return 60000 / (currentTime - lastTime);
	}
	
	var sum = 0;    // Starting sum before avg
	
	// Go through all time samples to add the total bpm of all samples, don't do last the oldest sample shouldn't be counted
	for(i = timeSampleArray.length-1; i>=1; i--)
	{
			// Sum the BPM
			sum = sum + calculateBPM(timeSampleArray[i-1], timeSampleArray[i] );
	}
	return ( sum/(timeSampleArray.length-1) );
}

var count = 0;
var timeSampleArray = [];     // Init to empty array
function rhythmPress()
{
	var timeSampleMaxSize = 5;
	var timeSeconds = new Date;
	var msecs = timeSeconds.getTime();     // Current milliseconds
	
	// Add current time to array
	timeSampleArray[timeSampleArray.length] = msecs;
	count++; // Increment press #
	
	// Check if amount of samples is enough to calculate bpm (at least 2), do nothing otherwise
	// could change document to say "calculating BPM"
	if( timeSampleArray.length > 1 ) {
		if(timeSampleArray.length > timeSampleMaxSize)
		{
			timeSampleArray.shift();        // Shift off the oldest value if there are more than 3 time samples
		}
		// Set the document area to display the bpm calculated
		document.getElementById('bpm').value = Math.round( calculateAverageBPM(timeSampleArray) );   // Round for easy reading, scope is array outside of function
	}
	else {
		document.getElementById('bpm').value = 'Calculating BPM';
	}
	
}


