/** 
 * click_helper.js
 *	Description: Web worker script used to play metronome sounds, should be passed
 *                    an audio buffer containing the metronome click sounds to play (hi and low).
 *                    Is called to terminate when the user clicks the stop button on the html page.
 *                    
 **/